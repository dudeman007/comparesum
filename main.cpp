#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <vector>

int main(int argc, char const *argv[]) {
	std::cout << "Analizing checksums" << std::endl;
	std::string arg1 = argv[1];
	std::string arg2 = argv[2];
	// concat the arguments eg. sha1 file_to_checksum
	std::string str = arg1 + " " + arg2 + " | cat >> checksum.dat";
	const char* result = str.c_str();


	std::system(result);

	std::ifstream fileIn("checksum.dat", std::ios::in);

	std::string checkSum;

	if (fileIn.is_open()) {
		std::getline(fileIn, checkSum, ' ');
		fileIn.close();
	}
	else {
		std::cerr << "Unable to find file " << argv[2] << std::endl;
	}

	if(argv[3] == checkSum){
		std::cout << argv[2] << ": [ GOOD ]" << std::endl;
	}
	else {
		std::cout << argv[2] << ": [ BAD ]" << std::endl;
	}

	std::remove("./checksum.dat");

	return 0;
}
